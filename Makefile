all:
	@protoc -I. -I$$GOPATH/src \
		-I$$GOPATH/src/github.com/googleapis/googleapis \
		--go-grpc_out=. --go_out=. v3.proto
	@protoc -I. -I$$GOPATH/src \
		-I$$GOPATH/src/github.com/googleapis/googleapis \
		--grpc-gateway_out=logtostderr=true:. v3.proto
	@python -m grpc_tools.protoc -I. \
		-I$$GOPATH/src/github.com/googleapis/googleapis \
		--python_out=. --grpc_python_out=. ./v3.proto
java:
	@protoc -I. \
		-I$$GOPATH/src/github.com/googleapis/googleapis \
		--java_out=. v3.proto

swift:
	@protoc -I. \
		-I$$GOPATH/src \
		-I$$GOPATH/src/github.com/googleapis/googleapis \
		--grpc-swift_out=Client=true,Server=false:. \
		--swift_out=. \
		v3.proto

clean:
	rm -rf *.go
	rm -rf *.js
	rm -rf *.ts
